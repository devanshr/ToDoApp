package com.devanshrastogi.webbackend.repository;

import com.devanshrastogi.webbackend.data.ToDo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ToDoRepository extends MongoRepository<ToDo, String> {
}
