package com.devanshrastogi.webbackend.service;

import com.devanshrastogi.webbackend.data.ToDo;
import com.devanshrastogi.webbackend.exception.EntityNotFoundException;
import com.devanshrastogi.webbackend.repository.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ToDoService {

    @Autowired
    private ToDoRepository toDoRepository;

    public List<ToDo> findAll(){
        return toDoRepository.findAll();
    }

    // if Id not found then return Exception using Optional of a ToDo
    public ToDo findById(String id){
        return toDoRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    public ToDo save(ToDo toDo){
        return toDoRepository.save(toDo);
    }

    public void deleteById(String id){
        toDoRepository.deleteById(id);
    }
}
